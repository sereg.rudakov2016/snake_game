// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"



UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		class UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		class ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)		
		TSubclassOf<class ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AFoodSpawner> FoodSpawnerClass;

	UPROPERTY(EditDefaultsOnly)
		TArray<class AFoodSpawner*> FoodSpawners;

	UPROPERTY(EditDefaultsOnly)
		int SizeGreedSpawner;

	UPROPERTY(EditDefaultsOnly)
		FVector StartLocSpawner;

	UPROPERTY(EditDefaultsOnly)
		float SpawnerStep;

	UPROPERTY(EditDefaultsOnly)
		int SpawnMax;

	UPROPERTY(BlueprintReadOnly)
		int SpawnMaxCurr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	void CreateSnakeActor();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);

	UFUNCTION()
		void HandlePlayerHorisontalInput(float value);

	UFUNCTION()
		void SpawnSpawnerFood();

	UFUNCTION()
		void SpawnFood(int Count, bool isStart=false);

	UFUNCTION(BlueprintCallable)
		void Start();

	UFUNCTION(BlueprintNativeEvent)
		void AddElement();
	void AddElement_Implementation();
};
