// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "FoodSpawner.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"
// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	if (TypeFood != ETypeFood::Block)
	{
		if (UKismetMathLibrary::RandomBool())
		{
			int Rand = UKismetMathLibrary::RandomIntegerInRange(0, 2);

			switch (Rand)
			{
			case 0:
				TypeFood = ETypeFood::AddElement;
				break;
			case 1:
				TypeFood = ETypeFood::AddSpeed;
				break;
			case 2:
				TypeFood = ETypeFood::Slow;
				break;
			default:
				break;
			}
		}
	}

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake))
		{

			switch (TypeFood)
			{
			case ETypeFood::AddElement:
			    Snake->AddSnakeElement(1);
				break;
			case ETypeFood::AddSpeed:
				Snake->ChangedSpeed(false);
				break;
			case ETypeFood::Slow:
				Snake->ChangedSpeed(true);
				break;
			case ETypeFood::Block:
				Snake->Destroy();
				break;
			default:
				break;
			}

			if (SelfSpawner) { SelfSpawner->isSpawn = false; }

			APlayerPawnBase* LocPlayer = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(this, 0));                     //::GetGameInstance(this)->GetSubsystem<UPlayerProfiler>();
			if (LocPlayer)
			{
				LocPlayer->SpawnMaxCurr--;
				LocPlayer->SpawnFood(UKismetMathLibrary::RandomIntegerInRange(1, 2));
			}
			Destroy();
		}
	}

}

